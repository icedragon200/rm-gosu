#
# rgss3_req.rb
#
dir    = File.dirname(__FILE__)
files  = Dir.glob(File.join(dir, 'rgss3', '**/*.{rb,so}'))
exempt = ['ext/RGSSAD.rb']

(files - exempt).each do |f|
  require_relative f
end  
