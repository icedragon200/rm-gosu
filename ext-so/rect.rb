#
# ext-so/rect.rb
#
# vr 1.0
require_relative 'rect/rgx3_rect.so'

class Rect

  class << self

    private(:new4)

    def new(*args)
      case args.size
      when 0
        x, y, w, h = 0, 0, 0, 0
      # // Normally impossible in RMVX/A  
      when 1
        r, = args
        raise(ArgumentError) unless r.kind_of?(Rect)
        x, y, w, h = r.as_ary
      when 4
        x, y, w, h = *args
      end  
      new4(x, y, w, h);
    end

  end

  private :initialize4, :set4

  def initialize(*args)
    initialize4(*args)
  end

  def set(*args)
    case args.size
    when 1
      r, = args
      raise(ArgumentError) unless r.kind_of?(Rect)
      x, y, w, h = r.as_ary
    when 4
      x, y, w, h = *args
    end  
    set4(x, y, w, h);
  end

  def to_s
    s = super
    s[0...(s.length-1)] + " [#{self.x}, #{self.y}, #{self.width}, #{self.height}]>"
  end

end
