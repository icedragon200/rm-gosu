#
# ext-so/table.rb
#
# vr 1.0
require_relative 'table/rgx3_table.so'

class Table
class << self

  # No access 4 you
  private :new1, :new2, :new3

  # Overload Fix
  def new(*args)
    case args.size
    when 1 ; new1(*args)
    when 2 ; new2(*args)
    when 3 ; new3(*args)
    else 
      raise(ArgumentError)
    end
  end
end

  VALUE_LIMIT_CEIL  = 0xFFFF
  VALUE_LIMIT_FLOOR = -0xFFFF

  private :initialize1, :initialize2, :initialize3
  private :get1, :get2, :get3
  private :set1, :set2, :set3

  def initialize(*args)
    case args.size
    when 1 ; initialize1(*args)
    when 2 ; initialize2(*args)
    when 3 ; initialize3(*args)
    else 
      raise(ArgumentError)
    end
  end

  def check_for_range(x, y=0, z=0)
    raise(ArgumentError, "X out of range: #{x} / #{xsize}") if x < 0 or x > xsize
    raise(ArgumentError, "Y out of range: #{y} / #{ysize}") if y < 0 or y > ysize
    raise(ArgumentError, "Z out of range: #{z} / #{zsize}") if z < 0 or z > zsize
    return true
  end

  private :check_for_range

  ## 
  # DO NOT BYPASS THESE (get/set)
  #
  # I'm no pro with C, and there is a lurking memory error if you go
  # outside of the set range
  def get(*args)
    case args.size
    when 1 
      x, = *args
      check_for_range(x)
      get1(x)
    when 2 
      x, y = *args
      check_for_range(x, y)
      get2(x, y)
    when 3  
      x, y, z = *args
      check_for_range(x, y, z)
      get3(x, y, z)
    else
      raise(ArgumentError)
    end
  end

  def set(*args)
    case args.size
    when 2 
      x, n = *args
      check_for_range(x)
      n = n < VALUE_LIMIT_FLOOR ? VALUE_LIMIT_FLOOR : (n > VALUE_LIMIT_CEIL ? VALUE_LIMIT_CEIL : n)
      set1(x, n)
    when 3 
      x, y, n = *args
      check_for_range(x, y)
      n = n < VALUE_LIMIT_FLOOR ? VALUE_LIMIT_FLOOR : (n > VALUE_LIMIT_CEIL ? VALUE_LIMIT_CEIL : n)
      set2(x, y, n)
    when 4 
      x, y, z, n = *args
      check_for_range(x, y, z)
      n = n < VALUE_LIMIT_FLOOR ? VALUE_LIMIT_FLOOR : (n > VALUE_LIMIT_CEIL ? VALUE_LIMIT_CEIL : n)
      set3(x, y, z, n)
    else
      raise(ArgumentError)
    end
  end

  def resize(*args)
    case args.size
    when 1 ; resize1(*args)
    when 2 ; resize2(*args)
    when 3 ; resize3(*args)
    else
      raise(ArgumentError)
    end
  end

  alias [] get
  alias []= set

  # // Temporary Until i can fix this
  def _dump(d = 0)
    s = [@dim, @xsize, @ysize, @zsize, @xsize * @ysize * @zsize].pack('LLLLL')
    a = []
    ta = []
    dt = self.to_a
    dt.each do |d|
      if d.is_a?(Fixnum) && (d < 32768 && d >= 0)
        s << [d].pack("S")
      else
        s << [ta].pack("S#{ta.size}")
        ni = a.size
        a << d
        s << [0x8000|ni].pack("S")
      end
    end
    if a.size > 0
      s << Marshal.dump(a)
    end
    s
  end
      
  def self._load(s)
    size, nx, ny, nz, items = *s[0, 20].unpack('LLLLL')
    t = new(*[nx, ny, nz][0,size])
    d = s[20, items * 2].unpack("S#{items}")
    if s.length > (20+items*2)
      a = Marshal.load(s[(20+items*2)...s.length])
      d.collect! do |i|
        if i & 0x8000 == 0x8000
          a[i&~0x8000]
        else
          i
        end
      end
    end
    set_tb_data_from(t, d)
    t
  end

end

