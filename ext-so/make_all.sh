#
# make_all.sh
#
set org_dir=$PWD

mdirs=( chuchu table rect color tone bitmap viewport sprite plane window tilemap )

# seperate .so's for use
for s in "${mdirs[@]}"
do
  cd $s
  #make clean
  make
  cd ..
done

# Compile the big boy
make
