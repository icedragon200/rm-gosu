/*

  tone.c

 */
#include "ruby.h"
#include "tone.h"

typedef struct rgx3_tone 
{
  double red, green, blue, grey;
} RGX3Tone;

VALUE rgx3_cTone = Qnil;

static void tn_set_struct(RGX3Tone *tn, double cred, double cgreen, double cblue, double cgrey)
{
  LIMTI_TONE_DOUBLE(cred);
  LIMTI_TONE_DOUBLE(cgreen);
  LIMTI_TONE_DOUBLE(cblue);
  LIMTI_TONE_DOUBLE(cgrey);

  tn->red = cred;
  tn->green = cgreen;
  tn->blue = cblue;
  tn->grey = cgrey;
}

static RGX3Tone *tn_struct_new(double cred, double cgreen, double cblue, double cgrey)
{
  RGX3Tone *tn;
  tn = malloc(sizeof(RGX3Tone));

  tn_set_struct(tn, cred, cgreen, cblue, cgrey);

  return tn;
}

static void tn_free(void *p)
{
  free(p);
}

static VALUE tn_new(VALUE klass, VALUE cred, VALUE cgreen, VALUE cblue, VALUE cgrey)
{
  VALUE argv[] = { cred, cgreen, cblue, cgrey };
  RGX3Tone *ptr = tn_struct_new(NUM2DOUBLE(cred), NUM2DOUBLE(cgreen), NUM2DOUBLE(cblue), NUM2DOUBLE(cgrey));
  VALUE tdata = Data_Wrap_Struct(klass, 0, tn_free, ptr);
  rb_obj_call_init(tdata, 4, argv);
  return tdata;
}

static VALUE tn_initialize(VALUE self, VALUE cred, VALUE cgreen, VALUE cblue, VALUE cgrey)
{
  return self;
}

static VALUE tn_set(VALUE self, VALUE cred, VALUE cgreen, VALUE cblue, VALUE cgrey)
{
  GET_TONE(self, tone);

  tn_set_struct(tone, NUM2DOUBLE(cred), NUM2DOUBLE(cgreen), NUM2DOUBLE(cblue), NUM2DOUBLE(cgrey));

  return self;
}

static VALUE tn_set_red(VALUE self, VALUE cred)
{
  GET_TONE(self, tone);

  double val = NUM2DOUBLE(cred);
  LIMTI_TONE_DOUBLE(val);

  tone->red = val;

  return DOUBLE2NUM(tone->red);
}

static VALUE tn_set_green(VALUE self, VALUE cgreen)
{
  GET_TONE(self, tone);

  double val = NUM2DOUBLE(cgreen);
  LIMTI_TONE_DOUBLE(val);

  tone->green = val;

  return DOUBLE2NUM(tone->green);
}

static VALUE tn_set_blue(VALUE self, VALUE cblue)
{
  GET_TONE(self, tone);

  double val = NUM2DOUBLE(cblue);
  LIMTI_TONE_DOUBLE(val);

  tone->blue = val;

  return DOUBLE2NUM(tone->blue);
}

static VALUE tn_set_grey(VALUE self, VALUE cgrey)
{
  GET_TONE(self, tone);

  double val = NUM2DOUBLE(cgrey);
  LIMTI_TONE_DOUBLE(val);

  tone->grey = val;

  return DOUBLE2NUM(tone->grey);
}

static VALUE tn_get_red(VALUE self)
{
  GET_TONE(self, tone);
  return DOUBLE2NUM(tone->red);
}

static VALUE tn_get_green(VALUE self)
{
  GET_TONE(self, tone);
  return DOUBLE2NUM(tone->green);
}

static VALUE tn_get_blue(VALUE self)
{
  GET_TONE(self, tone);
  return DOUBLE2NUM(tone->blue);
}

static VALUE tn_get_grey(VALUE self)
{
  GET_TONE(self, tone);
  return DOUBLE2NUM(tone->grey);
}

static VALUE tn_as_ary(VALUE self)
{
  GET_TONE(self, tone);

  VALUE ary = rb_ary_new();

  rb_ary_push(ary, DOUBLE2NUM(tone->red));
  rb_ary_push(ary, DOUBLE2NUM(tone->green));
  rb_ary_push(ary, DOUBLE2NUM(tone->blue));
  rb_ary_push(ary, DOUBLE2NUM(tone->grey));

  return ary;
}

void Init_rgx3_tone() 
{
  rgx3_cTone = rb_define_class("Tone", rb_cObject);

  rb_define_singleton_method(rgx3_cTone, "new4", tn_new, 4);
  rb_define_method(rgx3_cTone, "initialize4", tn_initialize, 4);

  rb_define_method(rgx3_cTone, "set4", tn_set, 4);

  rb_define_method(rgx3_cTone, "red", tn_get_red, 0);
  rb_define_method(rgx3_cTone, "green", tn_get_green, 0);
  rb_define_method(rgx3_cTone, "blue", tn_get_blue, 0);
  rb_define_method(rgx3_cTone, "grey", tn_get_grey, 0);

  rb_define_method(rgx3_cTone, "red=", tn_set_red, 1);
  rb_define_method(rgx3_cTone, "green=", tn_set_green, 1);
  rb_define_method(rgx3_cTone, "blue=", tn_set_blue, 1);
  rb_define_method(rgx3_cTone, "grey=", tn_set_grey, 1);

}
