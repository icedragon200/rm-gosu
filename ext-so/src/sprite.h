#ifndef INC_RGX3_SPRITE

  #define INC_RGX3_SPRITE
  
  #include "ruby.h"
  #include "rect.h"
  #include "bitmap.h"
  #include "viewport.h"

  void Init_rgx3_sprite();

#endif  
