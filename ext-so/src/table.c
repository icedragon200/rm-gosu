/*

  table.c

  rgx3_Table

  by IceDragon
  dc 18/11/2012
  dm 18/11/2012

  TODO:
    Add error handling.

 */
#include "ruby.h"    
#include "table.h"

typedef struct rgx3tb
{
  int tbdim, tbxsize, tbysize, tbzsize;
  int *tbdata;
} RGX3Table;

VALUE rgx3_cTable = Qnil;

static void tb_fill_with(int size, int *tb_data, int filler)
{
  int i;
  for(i = 0; i <= size; i++) 
  {
    tb_data[i] = filler;
  }
}

static RGX3Table *tb_struct_new(int dim, int xsize, int ysize, int zsize)
{
  int size = xsize * ysize * zsize;
  int *tb_data;
  tb_data = malloc(size * sizeof(int));

  tb_fill_with(size, tb_data, 0);

  RGX3Table *tb;
  tb = malloc(sizeof(RGX3Table));
  tb->tbdim = dim;
  tb->tbxsize = xsize;
  tb->tbysize = ysize;
  tb->tbzsize = zsize;
  tb->tbdata = tb_data;

  return tb;
}

// Get
static VALUE tb_get(VALUE self, VALUE x, VALUE y, VALUE z) {
  GET_TABLE(self);
  int xsize = ptr->tbxsize;
  int ysize = ptr->tbysize;
  int index = XYZ_TO_INDEX(NUM2INT(x), NUM2INT(y), NUM2INT(z), xsize, ysize);
  return INT2NUM(ptr->tbdata[index]);
}

static VALUE tb_get1(VALUE self, VALUE x) 
{
  return tb_get(self, x, INT2NUM(0), INT2NUM(0));
}

static VALUE tb_get2(VALUE self, VALUE x, VALUE y)
{
  return tb_get(self, x, y, INT2NUM(0));
}

static VALUE tb_get3(VALUE self, VALUE x, VALUE y, VALUE z)
{
  return tb_get(self, x, y, z);
}

// Set
static VALUE tb_set(VALUE self, VALUE x, VALUE y, VALUE z, VALUE value)
{
  GET_TABLE(self);
  int xsize = ptr->tbxsize;
  int ysize = ptr->tbysize;
  int index = XYZ_TO_INDEX(NUM2INT(x), NUM2INT(y), NUM2INT(z), xsize, ysize);
  ptr->tbdata[index] = NUM2INT(value);
  return INT2NUM(ptr->tbdata[index]);
}

static VALUE tb_set1(VALUE self, VALUE x, VALUE value) 
{
  return tb_set(self, x, INT2NUM(0), INT2NUM(0), value);
}

static VALUE tb_set2(VALUE self, VALUE x, VALUE y, VALUE value) 
{
  return tb_set(self, x, y, INT2NUM(0), value);
}

static VALUE tb_set3(VALUE self, VALUE x, VALUE y, VALUE z, VALUE value)
{ 
  return tb_set(self, x, y, z, value);  
}

// Ruby Interface
// Resize
static VALUE tb_resize(VALUE self, VALUE xsize, VALUE ysize, VALUE zsize)
{
  GET_TABLE(self);
  int orgsize = ptr->tbxsize * ptr->tbysize * ptr->tbzsize;
  int size = NUM2INT(xsize) * NUM2INT(ysize) * NUM2INT(zsize);

  int *orgtable = ptr->tbdata;
  int *temp = realloc(orgtable, size * sizeof(int));

  if(temp != NULL)
  {
    // table has expanded : Pad with 0s
    if(orgsize < size)
    {
      int i;
      for(i = orgsize; i < size; i++)
      {
        temp[i] = 0;
      }
    }

    ptr->tbxsize = NUM2INT(xsize);
    ptr->tbysize = NUM2INT(ysize);
    ptr->tbzsize = NUM2INT(zsize);
    ptr->tbdata = temp;
  }
  else
  {
    free(orgtable);
    rb_raise(rb_eException, "Table could not be resized");
    return Qnil;
  }
  return self;
}

static VALUE tb_resize1(VALUE self, VALUE xsize) 
{
  return tb_resize(self, xsize, INT2NUM(1), INT2NUM(1));
}

static VALUE tb_resize2(VALUE self, VALUE xsize, VALUE ysize)
{
  return tb_resize(self, xsize, ysize, INT2NUM(1));
}

static VALUE tb_resize3(VALUE self, VALUE xsize, VALUE ysize, VALUE zsize)
{
  return tb_resize(self, xsize, ysize, zsize);
}

static VALUE tb_datasize(VALUE self)
{
  GET_TABLE(self);
  return INT2NUM(ptr->tbxsize * ptr->tbysize * ptr->tbzsize);
  //return INT2NUM(sizeof(ptr->tbdata));
}

static int *get_tb_data(VALUE self)
{
  GET_TABLE(self); 
  return ptr->tbdata;
}

// Dimension
static VALUE tb_dim(VALUE self)
{
  GET_TABLE(self);
  return INT2NUM(ptr->tbdim);
}

// nsize
static VALUE tb_xsize(VALUE self)
{
  GET_TABLE(self);
  return INT2NUM(ptr->tbxsize);
}

static VALUE tb_ysize(VALUE self) 
{
  GET_TABLE(self);
  return INT2NUM(ptr->tbysize);
}

static VALUE tb_zsize(VALUE self) 
{
  GET_TABLE(self);
  return INT2NUM(ptr->tbzsize);
}

static VALUE tb_clear(VALUE self)
{
  tb_fill_with(NUM2INT(tb_datasize(self)), get_tb_data(self), 0);
  return Qtrue;
}

static void tb_free(void *p) 
{
  free(p);
}

// .new
static VALUE tb_new(VALUE klass, VALUE dim, VALUE xsize, VALUE ysize, VALUE zsize)
{
  VALUE argv[] = { xsize, ysize, zsize };
  RGX3Table *ptr = tb_struct_new(NUM2INT(dim), NUM2INT(xsize), NUM2INT(ysize), NUM2INT(zsize));
  VALUE tdata = Data_Wrap_Struct(klass, 0, tb_free, ptr);
  rb_obj_call_init(tdata, 3, argv);
  return tdata;
}

static VALUE tb_new1(VALUE klass, VALUE xsize)
{
  return tb_new(klass, INT2NUM(1), xsize, INT2NUM(1), INT2NUM(1));
}

static VALUE tb_new2(VALUE klass, VALUE xsize, VALUE ysize)
{
  return tb_new(klass, INT2NUM(2), xsize, ysize, INT2NUM(1));
}

static VALUE tb_new3(VALUE klass, VALUE xsize, VALUE ysize, VALUE zsize)
{
  return tb_new(klass, INT2NUM(3), xsize, ysize, zsize);
}

// #initialize
static VALUE tb_init1(VALUE self, VALUE xsize)
{
  return self;
}

static VALUE tb_init2(VALUE self, VALUE xsize, VALUE ysize)
{
  return self;
}

static VALUE tb_init3(VALUE self, VALUE xsize, VALUE ysize, VALUE zsize)
{
  return self;
}

static VALUE tb_to_a(VALUE self)
{
  GET_TABLE(self);

  VALUE ary = rb_ary_new();
  
  int size = ptr->tbxsize * ptr->tbysize * ptr->tbzsize;

  int *tb_data = ptr->tbdata;

  int i;
  for(i = 0; i < size; i++){
    rb_ary_push(ary, INT2NUM(tb_data[i])); 
  }
  
  return ary;
}

//
// set_tb_data_from(Table target, Array source)
//
static VALUE set_tb_data_from(VALUE klass, VALUE tb, VALUE ary)
{
  GET_TABLE(tb);
  
  int *tb_data = ptr->tbdata;
  VALUE newary = rb_ary_to_ary(ary);

  int size = tb_datasize(tb);
  
  VALUE num;
  VALUE zero = INT2NUM(0);

  int i;
  for(i = 0; i < size; i++)
  {
    if(rb_ary_length(newary) > 0) 
    {
      num = rb_ary_shift(newary);
      if(num == Qnil)
      {
        num = zero;
      }
    }
    else 
    {
      num = zero;
    }  

    tb_data[i] = NUM2INT(num);
  }
  
  return Qtrue;
}

static VALUE tb_replace(VALUE self, VALUE tb)
{
  RGX3Table *target_tb; 
  RGX3Table *source_tb; 
  Data_Get_Struct(self, RGX3Table, target_tb);
  Data_Get_Struct(tb, RGX3Table, source_tb);

  int xs = source_tb->tbxsize;
  int ys = source_tb->tbysize;
  int zs = source_tb->tbzsize;

  int size = NUM2INT(tb_datasize(self));

  tb_resize(self, INT2NUM(xs), INT2NUM(ys), INT2NUM(zs));
  target_tb->tbdim = source_tb->tbdim;

  int *tb_data = target_tb->tbdata;
  int *stb_data = source_tb->tbdata;

  int i;
  for(i = 0; i < size; i++)
  {
    tb_data[i] = stb_data[i];
  }

  return self;
}

void Init_rgx3_table() 
{
  rgx3_cTable = rb_define_class("Table", rb_cObject);
  rb_define_singleton_method(rgx3_cTable, "new1", tb_new1, 1);
  rb_define_singleton_method(rgx3_cTable, "new2", tb_new2, 2);
  rb_define_singleton_method(rgx3_cTable, "new3", tb_new3, 3);
  
  rb_define_method(rgx3_cTable, "initialize1", tb_init1, 1);
  rb_define_method(rgx3_cTable, "initialize2", tb_init2, 2);
  rb_define_method(rgx3_cTable, "initialize3", tb_init3, 3);

  rb_define_method(rgx3_cTable, "xsize", tb_xsize, 0);
  rb_define_method(rgx3_cTable, "ysize", tb_ysize, 0);
  rb_define_method(rgx3_cTable, "zsize", tb_zsize, 0);

  rb_define_method(rgx3_cTable, "get1", tb_get1, 1);
  rb_define_method(rgx3_cTable, "get2", tb_get2, 2);
  rb_define_method(rgx3_cTable, "get3", tb_get3, 3);  

  rb_define_method(rgx3_cTable, "set1", tb_set1, 2);
  rb_define_method(rgx3_cTable, "set2", tb_set2, 3);
  rb_define_method(rgx3_cTable, "set3", tb_set3, 4);    

  rb_define_method(rgx3_cTable, "resize1", tb_resize1, 1);
  rb_define_method(rgx3_cTable, "resize2", tb_resize2, 2);
  rb_define_method(rgx3_cTable, "resize3", tb_resize3, 3);    

  // Extended
  rb_define_singleton_method(rgx3_cTable, "set_tb_data_from", set_tb_data_from, 2);
  
  rb_define_method(rgx3_cTable, "clear", tb_clear, 0);
  rb_define_method(rgx3_cTable, "dimension", tb_dim, 0);
  rb_define_method(rgx3_cTable, "datasize", tb_datasize, 0);

  rb_define_method(rgx3_cTable, "to_a", tb_to_a, 0);

  rb_define_method(rgx3_cTable, "replace", tb_replace, 1);
}
