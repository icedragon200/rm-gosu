/*

  RGX3Rect

 */
#include "ruby.h"
#include "rect.h"

typedef struct rgx3rc
{
  int x, y, width, height;
} RGX3Rect ;

VALUE rgx3_cRect = Qnil;

static RGX3Rect *rect_struct_new(int rx, int ry, int rwidth, int rheight)
{
  RGX3Rect *rc;
  rc = malloc(sizeof(RGX3Rect));
  rc->x = rx;
  rc->y = ry;
  rc->width = rwidth;
  rc->height = rheight;

  return rc;
}

static void rect_free(void *p)
{
  free(p);
}

static VALUE 
rect_new(VALUE klass, VALUE sx, VALUE sy, VALUE swidth, VALUE sheight)
{
  VALUE argv[] = { sx, sy, swidth, sheight };
  RGX3Rect *ptr = rect_struct_new(NUM2INT(sx), NUM2INT(sy), NUM2INT(swidth), NUM2INT(sheight));
  VALUE tdata = Data_Wrap_Struct(klass, 0, rect_free, ptr);
  rb_obj_call_init(tdata, 4, argv);
  return tdata;
}

static VALUE rect_initialize(VALUE self, VALUE sx, VALUE sy, VALUE swidth, VALUE sheight) 
{
  return self;
}

static VALUE rect_get_x(VALUE self)
{
  GET_RECT(self, rect);
  return INT2NUM(rect->x);
}

static VALUE rect_get_y(VALUE self)
{
  GET_RECT(self, rect);
  return INT2NUM(rect->y);
}

static VALUE rect_get_width(VALUE self)
{
  GET_RECT(self, rect);
  return INT2NUM(rect->width);
}

static VALUE rect_get_height(VALUE self)
{
  GET_RECT(self, rect);
  return INT2NUM(rect->height);
}

static VALUE rect_set_x(VALUE self, VALUE sx)
{
  GET_RECT(self, rect);
  rect->x = NUM2INT(sx);
  return INT2NUM(rect->x);
}

static VALUE rect_set_y(VALUE self, VALUE sy)
{
  GET_RECT(self, rect);
  rect->y = NUM2INT(sy);
  return INT2NUM(rect->y);
}

static VALUE rect_set_width(VALUE self, VALUE swidth)
{
  GET_RECT(self, rect);
  rect->width = NUM2INT(swidth);
  return INT2NUM(rect->width);
}

static VALUE rect_set_height(VALUE self, VALUE sheight)
{
  GET_RECT(self, rect);
  rect->height = NUM2INT(sheight);
  return INT2NUM(rect->height);
}

static VALUE 
rect_set(VALUE self, VALUE sx, VALUE sy, VALUE swidth, VALUE sheight)
{
  GET_RECT(self, rect);

  rect->x = NUM2INT(sx);
  rect->y = NUM2INT(sy);
  rect->width = NUM2INT(swidth);
  rect->height = NUM2INT(sheight);

  return self;
}

static VALUE rect_empty(VALUE self)
{
  GET_RECT(self, rect);

  rect->x = 0;
  rect->y = 0;
  rect->width = 0;
  rect->height = 0;

  return self;
}

static VALUE rect_as_ary(VALUE self)
{
  GET_RECT(self, rect);

  VALUE ary = rb_ary_new();

  rb_ary_push(ary, INT2NUM(rect->x));
  rb_ary_push(ary, INT2NUM(rect->y));
  rb_ary_push(ary, INT2NUM(rect->width));
  rb_ary_push(ary, INT2NUM(rect->height));

  return ary;
}

void Init_rgx3_rect()
{
  rgx3_cRect = rb_define_class("Rect", rb_cObject);

  rb_define_singleton_method(rgx3_cRect, "new4", rect_new, 4);

  rb_define_method(rgx3_cRect, "initialize4", rect_initialize, 4);
  rb_define_method(rgx3_cRect, "set4", rect_set, 4);

  rb_define_method(rgx3_cRect, "empty", rect_empty, 0);

  rb_define_method(rgx3_cRect, "x", rect_get_x, 0);
  rb_define_method(rgx3_cRect, "y", rect_get_y, 0);
  rb_define_method(rgx3_cRect, "width", rect_get_width, 0);
  rb_define_method(rgx3_cRect, "height", rect_get_height, 0);

  rb_define_method(rgx3_cRect, "x=", rect_set_x, 1);
  rb_define_method(rgx3_cRect, "y=", rect_set_y, 1);
  rb_define_method(rgx3_cRect, "width=", rect_set_width, 1);
  rb_define_method(rgx3_cRect, "height=", rect_set_height, 1);

  // Extended
  rb_define_method(rgx3_cRect, "as_ary", rect_as_ary, 0);

}
