/*
  rgx3.c
 */
#ifndef INC_RGX3_MAIN

  #define INC_RGX3_MAIN

  #include "ruby.h"
  
  #include "table.h" 
  #include "rect.h" 

  #include "color.h" 
  #include "tone.h" 
  
  #include "bitmap.h" 

  #include "viewport.h" 

  #include "sprite.h" 
  #include "plane.h" 
  #include "tilemap.h" 
  #include "window.h" 

#endif
