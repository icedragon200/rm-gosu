#include "ruby.h"
#include "color.h"

typedef struct rgx3_color
{
  double red, green, blue, alpha;
} RGX3Color;

VALUE rgx3_cColor = Qnil;

static void col_set_struct(RGX3Color *col, double cred, double cgreen, double cblue, double calpha)
{
  LIMIT_COLOR_DOUBLE(cred);
  LIMIT_COLOR_DOUBLE(cgreen);
  LIMIT_COLOR_DOUBLE(cblue);
  LIMIT_COLOR_DOUBLE(calpha);

  col->red = cred;
  col->green = cgreen;
  col->blue = cblue;
  col->alpha = calpha;
}

static RGX3Color *col_struct_new(double cred, double cgreen, double cblue, double calpha)
{
  RGX3Color *col;
  col = malloc(sizeof(RGX3Color));

  col_set_struct(col, cred, cgreen, cblue, calpha);

  return col;
}

static void col_free(void *p)
{
  free(p);
}

static VALUE col_new(VALUE klass, VALUE cred, VALUE cgreen, VALUE cblue, VALUE calpha)
{
  VALUE argv[] = { cred, cgreen, cblue, calpha };
  RGX3Color *ptr = col_struct_new(NUM2DOUBLE(cred), NUM2DOUBLE(cgreen), NUM2DOUBLE(cblue), NUM2DOUBLE(calpha));
  VALUE tdata = Data_Wrap_Struct(klass, 0, col_free, ptr);
  rb_obj_call_init(tdata, 4, argv);
  return tdata;
}

static VALUE col_initialize(VALUE self, VALUE cred, VALUE cgreen, VALUE cblue, VALUE calpha)
{
  return self;
}

static VALUE col_set(VALUE self, VALUE cred, VALUE cgreen, VALUE cblue, VALUE calpha)
{
  GET_COLOR(self, color);

  col_set_struct(color, NUM2DOUBLE(cred), NUM2DOUBLE(cgreen), NUM2DOUBLE(cblue), NUM2DOUBLE(calpha));

  return self;
}

static VALUE col_set_red(VALUE self, VALUE cred)
{
  GET_COLOR(self, color);

  double val = NUM2DOUBLE(cred);
  LIMIT_COLOR_DOUBLE(val);

  color->red = val;

  return DOUBLE2NUM(color->red);
}

static VALUE col_set_green(VALUE self, VALUE cgreen)
{
  GET_COLOR(self, color);

  double val = NUM2DOUBLE(cgreen);
  LIMIT_COLOR_DOUBLE(val);

  color->green = val;

  return DOUBLE2NUM(color->green);
}

static VALUE col_set_blue(VALUE self, VALUE cblue)
{
  GET_COLOR(self, color);

  double val = NUM2DOUBLE(cblue);
  LIMIT_COLOR_DOUBLE(val);

  color->blue = val;

  return DOUBLE2NUM(color->blue);
}

static VALUE col_set_alpha(VALUE self, VALUE calpha)
{
  GET_COLOR(self, color);

  double val = NUM2DOUBLE(calpha);
  LIMIT_COLOR_DOUBLE(val);

  color->alpha = val;

  return DOUBLE2NUM(color->alpha);
}

static VALUE col_get_red(VALUE self)
{
  GET_COLOR(self, color);
  return DOUBLE2NUM(color->red);
}

static VALUE col_get_green(VALUE self)
{
  GET_COLOR(self, color);
  return DOUBLE2NUM(color->green);
}

static VALUE col_get_blue(VALUE self)
{
  GET_COLOR(self, color);
  return DOUBLE2NUM(color->blue);
}

static VALUE col_get_alpha(VALUE self)
{
  GET_COLOR(self, color);
  return DOUBLE2NUM(color->alpha);
}

static VALUE col_as_ary(VALUE self)
{
  GET_COLOR(self, color);

  VALUE ary = rb_ary_new();

  rb_ary_push(ary, DOUBLE2NUM(color->red));
  rb_ary_push(ary, DOUBLE2NUM(color->green));
  rb_ary_push(ary, DOUBLE2NUM(color->blue));
  rb_ary_push(ary, DOUBLE2NUM(color->alpha));

  return ary;
}

void Init_rgx3_color()
{
  rgx3_cColor = rb_define_class("Color", rb_cObject);

  rb_define_singleton_method(rgx3_cColor, "new4", col_new, 4);

  rb_define_method(rgx3_cColor, "initialize4", col_initialize, 4);

  rb_define_method(rgx3_cColor, "set4", col_set, 4);

  rb_define_method(rgx3_cColor, "red"  , col_get_red, 0);
  rb_define_method(rgx3_cColor, "green", col_get_green, 0);
  rb_define_method(rgx3_cColor, "blue" , col_get_blue, 0);
  rb_define_method(rgx3_cColor, "grey" , col_get_alpha, 0);

  rb_define_method(rgx3_cColor, "red="  , col_set_red, 1);
  rb_define_method(rgx3_cColor, "green=", col_set_green, 1);
  rb_define_method(rgx3_cColor, "blue=" , col_set_blue, 1);
  rb_define_method(rgx3_cColor, "alpha=", col_set_alpha, 1);

  // Extended
  rb_define_method(rgx3_cColor, "as_ary", col_as_ary, 0);
}
