/*
 */
#include "rgx3.h"
#include "ruby.h"

void Init_rgx3()
{
  Init_rgx3_table();
  Init_rgx3_rect();

  Init_rgx3_color();
  Init_rgx3_tone();

  Init_rgx3_font();

  Init_rgx3_bitmap();

  Init_rgx3_viewport();

  Init_rgx3_sprite();
  Init_rgx3_plane();
  Init_rgx3_tilemap();
  Init_rgx3_window();

  Init_chuchu();
}
