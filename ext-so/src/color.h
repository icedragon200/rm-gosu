#ifndef INC_RGX3_COLOR

  #define INC_RGX3_COLOR

  #include "ruby.h"

  // Constants
  #define COLOR_LIMIT_FLOOR 0.0
  #define COLOR_LIMIT_CEIL 255.0

  // MACROS
  #define GET_COLOR(self, color) RGX3Color *color; Data_Get_Struct(self, RGX3Color, color)
  #define LIMIT_COLOR_DOUBLE(dub) if(dub < COLOR_LIMIT_FLOOR) { dub = COLOR_LIMIT_FLOOR; } else if(dub > COLOR_LIMIT_CEIL) { dub = COLOR_LIMIT_CEIL; }

  void Init_rgx3_color();

#endif  

