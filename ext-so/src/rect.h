#ifndef INC_RGX3_RECT

  #define INC_RGX3_RECT

  #include "ruby.h"

  // MACROS  
  #define GET_RECT(self, rect) RGX3Rect *rect; Data_Get_Struct(self, RGX3Rect, rect)

  void Init_rgx3_rect();

#endif  
