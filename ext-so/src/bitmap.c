#include "ruby.h"
#include "bitmap.h"

VALUE rgx3_cBitmap = Qnil;

void Init_rgx3_bitmap()
{
  rgx3_cBitmap = rb_define_class("Bitmap", rb_cObject);

  rb_define_singleton_method(rgx3_cBitmap, "new2", bmp_new2, 2);
  rb_define_singleton_method(rgx3_cBitmap, "new1", bmp_new1, 2);
  
  rb_define_method(rgx3_cBitmap, "initialize2", bmp_initialize2, 2);
  rb_define_method(rgx3_cBitmap, "initialize1", bmp_initialize1, 1);
  //rb_define_method(rgx3_cBitmap, "initialize", bmp_initialize, 1);

  rb_define_method(rgx3_cBitmap, "dispose", bmp_dispose, 0);
  rb_define_method(rgx3_cBitmap, "was_disposed", bmp_was_disposed, 0);

  rb_define_method(rgx3_cBitmap, "get_width", bmp_get_width, 0);
  rb_define_method(rgx3_cBitmap, "get_height", bmp_get_height, 0);
  rb_define_method(rgx3_cBitmap, "get_rect", bmp_get_rect, 0);

  rb_define_method(rgx3_cBitmap, "blit5", bmp_blit5, 5);
  rb_define_method(rgx3_cBitmap, "stretch_blt4", bmp_stretch_blt4, 4);
  rb_define_method(rgx3_cBitmap, "fill_rect5", bmp_fill_rect5, 5);
  rb_define_method(rgx3_cBitmap, "fill_rect2", bmp_fill_rect2, 2);
  rb_define_method(rgx3_cBitmap, "gradient_fill_rect7", bmp_gradient_fill_rect7, 7);
  rb_define_method(rgx3_cBitmap, "gradient_fill_rect4", bmp_gradient_fill_rect4, 4);

  rb_define_method(rgx3_cBitmap, "clear", bmp_clear, 0);
  rb_define_method(rgx3_cBitmap, "clear_rect4", bmp_clear_rect4, 4);
  rb_define_method(rgx3_cBitmap, "clear_rect1", bmp_clear_rect1, 1);

  rb_define_method(rgx3_cBitmap, "get_pixel", bmp_get_pixel, 2);
  rb_define_method(rgx3_cBitmap, "set_pixel", bmp_set_pixel, 3);

  rb_define_method(rgx3_cBitmap, "hue_change", bmp_hue_change, 1);

  rb_define_method(rgx3_cBitmap, "blur", bmp_blur, 0);
  rb_define_method(rgx3_cBitmap, "radial_blur", bmp_radial_blur, 2);

  rb_define_method(rgx3_cBitmap, "draw_text6", bmp_draw_text6, 6);
  rb_define_method(rgx3_cBitmap, "draw_text3", bmp_draw_text3, 3);

  rb_define_method(rgx3_cBitmap, "text_size", bmp_text_size, 1);

  // Property
  rb_define_method(rgx3_cBitmap, "get_font", bmp_get_font, 0);
}
