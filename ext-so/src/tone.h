#ifndef INC_RGX3_TONE

  #define INC_RGX3_TONE
  
  #include "ruby.h"

  // Constants
  #define TONE_LIMIT_FLOOR -255.0
  #define TONE_LIMIT_CEIL 255.0

  // MACROS
  #define GET_TONE(self, tone) RGX3Tone *tone; Data_Get_Struct(self, RGX3Tone, tone)
  #define LIMIT_TONE_DOUBLE(dub) if(dub < TONE_LIMIT_FLOOR) { dub = TONE_LIMIT_FLOOR; } else if(dub > TONE_LIMIT_CEIL) { dub = TONE_LIMIT_CEIL; }

  void Init_rgx3_tone();

#endif  
