#ifndef INC_RGX3_BITMAP
  
  #define INC_RGX3_BITMAP

  #include "ruby.h"
  #include "rect.h"

  void Init_rgx3_bitmap();

  // Functions
  VALUE bmp_new1(VALUE klass, VALUE path);
  VALUE bmp_new2(VALUE klass, VALUE width, VALUE height);

  VALUE bmp_initialize2(VALUE self, VALUE width, VALUE height);
  VALUE bmp_initialize1(VALUE self, VALUE path);

  VALUE bmp_dispose(VALUE self);
  VALUE bmp_was_disposed(VALUE self);

  VALUE bmp_get_width(VALUE self);
  VALUE bmp_get_height(VALUE self);
  VALUE bmp_get_rect(VALUE self);

  VALUE bmp_blit5(VALUE self, 
    VALUE x, VALUE y, VALUE src_bitmap, VALUE src_rect, VALUE opacity);
  VALUE bmp_stretch_blt4(VALUE self, 
    VALUE dest_rect, VALUE src_bitmap, VALUE src_rect, VALUE opacity);
  VALUE bmp_fill_rect5(VALUE self, 
    VALUE x, VALUE y, VALUE width, VALUE height, VALUE color);
  VALUE bmp_fill_rect2(VALUE self, 
    VALUE rect, VALUE color);
  VALUE bmp_gradient_fill_rect7(VALUE self, 
    VALUE x, VALUE y, VALUE width, VALUE height, 
    VALUE color1, VALUE color2, VALUE vertical);
  VALUE bmp_gradient_fill_rect4(VALUE self,
   VALUE rect, VALUE color1, VALUE color2, VALUE vertical);

  VALUE bmp_clear(VALUE self);
  VALUE bmp_clear_rect4(VALUE self, 
    VALUE x, VALUE y, VALUE width, VALUE height);
  VALUE bmp_clear_rect1(VALUE self, VALUE rect);

  VALUE bmp_get_pixel(VALUE self, VALUE x, VALUE y);
  VALUE bmp_set_pixel(VALUE self, VALUE x, VALUE y, VALUE color);

  VALUE bmp_hue_change(VALUE self, VALUE hue);

  VALUE bmp_blur(VALUE self, VALUE blur);
  VALUE bmp_radial_blur(VALUE self, VALUE angle, VALUE division);

  VALUE bmp_draw_text6(VALUE self, 
    VALUE x, VALUE y, VALUE width, VALUE height, VALUE str, VALUE align);
  VALUE bmp_draw_text3(VALUE self, VALUE rect, VALUE str, VALUE align);

  VALUE bmp_text_size(VALUE self, VALUE str);

  // Property
  VALUE bmp_get_font(VALUE self);

#endif  
