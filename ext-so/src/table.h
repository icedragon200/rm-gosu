/*
  table.h
  
 */
#ifndef INC_RGX3_TABLE

  #define INC_RGX3_TABLE

  #include "ruby.h"
  
  // MACROS
  #define GET_TABLE(self) RGX3Table *ptr; Data_Get_Struct(self, RGX3Table, ptr)
  #define XYZ_TO_INDEX(x, y, z, xsize, ysize) (x + (y * xsize) + (z * ysize))

  // Forward Decl.
  void Init_rgx3_table(); 

#endif
