RM-GOSU
=======

## IceDragon
All ext-so's where written and compiled under Arch Linux x86_64.

### Available Extensions
```
Table     (Ruby & C-Ext)
Color     (Ruby & C-Ext)
Tone      (Ruby & C-Ext)
Rect      (Ruby & C-Ext)
Font      (Ruby)
RGSSError (Ruby)
RGSSReset (Ruby)
```

### TODO
```
Viewport

Plane
Sprite
Window
Tilemap

Bitmap
```

## Original
Will add details later.

### Uses:

- zlib
- oily_png
- gosu

### Extra Credits:

- Yeyinde (RGSSAD module, Table class)
- RTH (RGSS1 Tilemap)
- Enterbrain (Maker of RPG Maker)
